package com.afs.restapi.advice;

public class ErrorResponse {
    private final int statuscode;

    private final String message;

    public ErrorResponse(int statuscode, String message) {
        this.statuscode = statuscode;
        this.message = message;
    }

    public int getStatuscode() {
        return statuscode;
    }


    public String getMessage() {
        return message;
    }

}
